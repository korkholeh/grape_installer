from .base import BaseInstaller


class BasicSiteInstaller(BaseInstaller):
    description = "Basic site"

    def __init__(self):
        print("Basic site constructor")

config_class = BasicSiteInstaller
